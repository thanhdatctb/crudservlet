<%-- 
    Document   : list
    Created on : Dec 9, 2020, 3:04:43 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Danh sách nhân viên</h1>
        <a href="create.jsp">Thêm mới</a>
        <br/>
        <form action="Search">
            <input type="text" name ="keyword"/>
            <input type="submit" value="Tìm kiếm"/>
        </form>
        <c:if test="${empty nvs}">
            <h3>Không có dữ liệu</h3>
        </c:if>
        <c:if test="${not empty nvs}">
            <table border="1">
                <tr>
                    <th>Id</th>
                    <th>Tên</th>
                    <th>Năm sinh</th>
                    <th>Địa chỉ</th>
                    <th>Phòng ban</th>
                    <th>Sửa</th>
                    <th>Xóa</th>
                </tr>
                <%
                  //System.out.println(${nvs});  
                %>
                <c:forEach items="${nvs}" var="nv">
                    <tr>
                        <td>${nv.id}</td>
                        <td>${nv.name}</td>
                        <td>${nv.birthyear}</td>
                        <td>${nv.address}</td>
                        <td>${nv.department}</td>
                        <td>
                            <a href="BeforeUpdate?id=${nv.id}">Sửa</a>
                        </td>
                        <td>
                            <a href="Delete?id=${nv.id}">Xóa</a>
                        </td>
                    </tr>


                </c:forEach>
            </table>
        </c:if>
    </body>
</html>
