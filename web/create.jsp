<%-- 
    Document   : create
    Created on : Dec 10, 2020, 11:31:41 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Thêm nhân viên</h1>
        <form  action = "Create">
            <table>
                <tr>
                    <td>Tên</td>
                    <td><input type="text" name ="name"></td>
                </tr>
                <tr>
                    <td>Năm sinh</td>
                    <td><input type="number" name ="year"></td>
                </tr>
                <tr>
                    <td>Địa chỉ</td>
                    <td><input type="text" name ="address"></td>
                </tr>
                <tr>
                    <td>Phòng ban</td>
                    <td><input type="text" name ="department"></td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Thêm">
                    </td>
                </tr>
            </table>
        </form>

    </body>
</html>
