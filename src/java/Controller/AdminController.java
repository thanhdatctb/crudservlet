/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Admin;
import Model.Sessioni;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author Admin
 */
public class AdminController {
    private EntityManager em = Persistence.createEntityManagerFactory("qlnsPU").createEntityManager();
    public boolean LogIn(Admin admin){
        Admin ad = admin;
        String hql = String.format("select a from %s a where a.username = :uname and a.password = :pass", Admin.class.getName());
        List<Admin> admins = em.createQuery(hql)
                .setParameter("uname", admin.getUsername())
                .setParameter("pass", admin.getPassword())
                .getResultList();
        if(admins.size() == 0 )
               return false;
        new Sessioni().setAdmin(admin);
        return true;
  
    }
}
