/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Nhanvien;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author thanh
 */
public class NhanvienController {
    private EntityManager em = Persistence.createEntityManagerFactory("qlnsPU").createEntityManager();
     public List<Nhanvien> viewAll(){
        String hql = String.format("select a from %s a", Nhanvien.class.getName());
        return em.createQuery(hql).getResultList();
    }
    public boolean Add(Nhanvien nhanvien){
        em.getTransaction().begin();
        em.persist(nhanvien);
        em.getTransaction().commit();
        return true;
    }
    public boolean Edit(Nhanvien nhanvien){
        em.getTransaction().begin();
        em.merge(nhanvien);
        em.getTransaction().commit();
        return true;
    }
    public Nhanvien GetById(int id){
       return em.find(Nhanvien.class, id);
    }
    public boolean Delete(int id){
        em.getTransaction().begin();
        em.remove(this.GetById(id));
        em.getTransaction().commit();
        return true;
    }
    public List<Nhanvien> find(String keyword){
        String hql = String.format("select a from %s a where a.name like  :tennv ", Nhanvien.class.getName());
        return em.createQuery(hql)
                .setParameter("tennv", "%"+keyword+"%")
                .getResultList();
    }
}
